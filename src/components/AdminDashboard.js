import {useState,useEffect,useContext} from 'react';

import UserContext from '../userContext';

import {Table,Button,Col,Row} from 'react-bootstrap';

import {Link} from 'react-router-dom';

export default function AdminDashboard(){

	const {user} = useContext(UserContext);

	const [allProducts,setAllProducts] = useState([]);

	function archive (productId){

		console.log(productId);

		fetch(`https://dry-citadel-77970.herokuapp.com/products/archive/${productId}`,{

			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);
			window.location.href="/products";

		})

	}

	function activate (productId){

		console.log(productId);

		fetch(`https://dry-citadel-77970.herokuapp.com/products/activate/${productId}`,{

			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);
			window.location.href="/products";

		})

	}

	useEffect(()=>{

		if(user.isAdmin){

			fetch('https://dry-citadel-77970.herokuapp.com/products/',{
				headers: {
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {

				setAllProducts(data.map(product => {

					return (

						<tr key={product._id}>
							<td>{product.name}</td>
							<td>{product.description}</td>
							<td className="text-center">&#8369;{product.price}</td>
							<td className="text-center">{product.isActive ? "Available" : "Unavailable"}</td>
							<td className="text-center">
								<Link to={`/products/UpdateProduct/${product._id}`} className="btn btn-dark mx-1 my-1">Update</Link>
								{
									product.isActive
									?
									<Button variant="danger" className="mx-1 my-1" onClick={()=>{archive(product._id)}}>Disable</Button>
									:
									<Button variant="primary" className="mx-1 my-1" onClick={()=>{activate(product._id)}}>Enable</Button>
								}
							</td>
						</tr>)
				}))

			})
		}

	},[user.isAdmin])

	return (

		<>
		<h1 className="mt-5 text-center">Admin Dashboard</h1>
		<Row className="justify-content-center">
			<Col xs={12} md={6} lg ={3}>
				<Link to="/addproduct" className="mb-3 ms-4 btn btn-primary">Add Product</Link>
				<Link to="/orders" className="mb-3 mx-2 btn btn-success">View All Orders</Link>
			</Col>
		</Row>
		<Table striped bordered hover responsive>
			<thead className="bg-dark text-light">
				<tr>
					<th>Name</th>
					<th>Description</th>
					<th className="text-center">Price</th>
					<th className="text-center">Availability</th>
					<th className="text-center">Actions</th>
				</tr>
			</thead>
			<tbody>
					{allProducts}
			</tbody>
		</Table>
		</>
	)

}