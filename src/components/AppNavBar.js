import {useContext} from 'react';

import {Nav,Navbar,Container} from 'react-bootstrap'

import UserContext from '../userContext';

import {Link} from 'react-router-dom';

export default function AppNavBar(){

	const {user} = useContext(UserContext);
	console.log(user);

	return (

			<Navbar bg="primary" expand="lg" className="sticky-top navbar-dark bg-dark">
				<Container fluid>
					<Navbar.Brand href="/">ALXS e-Shop</Navbar.Brand>
					<Navbar.Toggle aria-controls="basic-navbar-nav" />
					<Navbar.Collapse id="basic-navbar-nav">
						<Nav className="ms-auto">
							<Link to="/" className="nav-link">Home</Link>
							{
								user.id
								? 
									user.isAdmin
									?
									<>
									<Link to="/logout" className="nav-link">Logout</Link>
									</>
									:
									<>
									<Link to="/products" className="nav-link">Products</Link>
									<Link to="/logout" className="nav-link">Logout</Link>
									</>
								:
								<>
									<Link to="/products" className="nav-link">Products</Link>
									<Link to="/register" className="nav-link">Register</Link>
									<Link to="/login" className="nav-link">Login</Link>
								</>
							}
						</Nav>
					</Navbar.Collapse>
				</Container>
			</Navbar>


		)

}