import {Row,Col,Card} from 'react-bootstrap';

export default function Highlights(){

	return (

			<Row>
				<Col xs={12} md={4}>
					<Card className="p-3 cardHighlight">
						<Card.Body>
							<img src="https://cdn4.iconfinder.com/data/icons/hardware-line/512/mice-mouse-computer-device-accessories-peripherals-gaming-512.png" className="imagePhoto mb-3 text-center"></img>	
							<Card.Title>
								<h2>Gaming Peripherals</h2>
							</Card.Title>
							<Card.Text>
								Non est fugiat dolore officia id labore laborum in enim mollit consequat sint quis in non ea quis fugiat.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className="p-3 cardHighlight">
						<Card.Body>
							<img src="https://cdn4.iconfinder.com/data/icons/hardware-line/512/headphones-microphone-computer-accessories-peripherals-gaming-communicate-512.png" className="imagePhoto mb-3 text-center"></img>
							<Card.Title>
								<h2>Office Solutions</h2>
							</Card.Title>
							<Card.Text>
								Labore exercitation ut esse labore ut excepteur sit adipisicing in adipisicing labore dolore occaecat elit reprehenderit do ad fugiat in cillum nostrud ut excepteur ea laborum mollit magna et ullamco excepteur elit dolor dolor.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className="p-3 cardHighlight">
						<Card.Body>
							<img src="https://cdn4.iconfinder.com/data/icons/hardware-line/512/ipad-tablet-mac-apple-device-hardware-mobile-512.png" className="imagePhoto mb-3 text-center"></img>
							<Card.Title>
								<h2>Mobile Needs</h2>
							</Card.Title>
							<Card.Text>
								Aliqua in eiusmod pariatur fugiat cupidatat incididunt veniam deserunt consectetur exercitation ad enim culpa nisi duis nostrud commodo dolor dolor proident id dolore id duis dolore tempor mollit tempor irure in.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
			</Row>

		)

}