import {Col,Card} from 'react-bootstrap';

import {Link} from 'react-router-dom';

export default function ProductCard({productProp}){

	console.log(productProp);

	return (
			<Col xs={12} md={4}>			
				<Card className="p-3 cardHighlight">
					<Card.Img variant="top" src="https://nayemdevs.com/wp-content/uploads/2020/03/default-product-image.png" />
					<Card.Body>
						<Card.Title>
							{productProp.name}
						</Card.Title>
						<Card.Text>
							{productProp.description}
						</Card.Text>
						<Card.Text>
							Price: &#8369;{productProp.price}
						</Card.Text>
						<Link to={`/products/ViewProduct/${productProp._id}`} className="btn btn-dark">View Product</Link>
					</Card.Body>
				</Card>
			</Col>
		)

}
