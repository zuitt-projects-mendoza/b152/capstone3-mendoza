import {useState,useContext,useEffect} from 'react';

import {Form,Button,Col,Row} from 'react-bootstrap';

import Swal from 'sweetalert2';

import UserContext from '../userContext';

import {Navigate} from 'react-router-dom';

export default function AddProduct(){

	const {user} = useContext(UserContext);

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");

	const [isActive,setIsActive] = useState(false);

	useEffect(()=>{

		if(name !== "" && description !== "" && price !== ""){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[name,description,price])

	function createProduct(e){

		e.preventDefault();

		let token = localStorage.getItem('token');

		fetch('https://dry-citadel-77970.herokuapp.com/products/',{

			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({

				name: name,
				description: description,
				price: price

			})

		})

		.then(res => res.json())
		.then(data => {

			console.log(data);
					
			if(data._id){
				Swal.fire({

					icon: "success",
					title: "Product Creation Successful",
				})

				window.location.href = "/products";

			} else {
				Swal.fire({

					icon: "error",
					title: "Product Creation Failed.",
					text: data.message
					
				})
			}

		})

	}

	return (
		user.isAdmin
		?
		<Row className="mt-5 justify-content-center">
			<Col xs={12} md={7}>
			<>
			<h1 className="my-5 text-center">Add Product</h1>
			<Form onSubmit={e =>createProduct(e)}>
				<Form.Group>
					<Form.Label>Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter Product Name" required value={name} onChange={e =>{setName(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Description:</Form.Label>
					<Form.Control type="text" placeholder="Enter Product Description" required value={description} onChange={e =>{setDescription(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Price:</Form.Label>
					<Form.Control type="number" placeholder="Enter Price" required value={price} onChange={e =>{setPrice(e.target.value)}}/>
				</Form.Group>
				{
					isActive
					?
					<Button variant="primary" type="submit" className="my-5">Submit</Button>
					:
					<Button variant="dark" type="submit" className="my-5" disabled>Submit</Button>
				}	
			</Form>
			</>
			</Col>
		</Row>

		:
		<Navigate to="/login" replace={true} />
	)
}