import {useContext} from 'react';
import Carousel from 'react-bootstrap/Carousel'
import Banner from '../components/Banner';
import Highlights from '../components/Highlights'

import AdminDashboard from '../components/AdminDashboard';

import UserContext from '../userContext';

export default function Home(){

	const {user} = useContext(UserContext);

	let bannerData = {

		title: "ALXS e-Shop",
		description: "Your One-stop shop for all your e-Related needs!",
		buttonText: "View Our Products",
		destination: "/products"

	}

	return (

		user.isAdmin
		?
		<AdminDashboard />

		:

		<>

<Carousel fade className="mt-5">
  <Carousel.Item>
    <img
      className="d-block w-100 carouselImg"
      src="https://cdn.shopify.com/s/files/1/0228/7629/1136/files/accbanner_1600x.png?v=1586283657"
      alt="First slide"
    />
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100 carouselImg"
      src="https://i.ibb.co/FDSkLbh/sagfsagsa.jpg"
      alt="Second slide"
    />
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100 carouselImg"
      src="https://i.ibb.co/VtCDDyt/sagfsagsa.jpg"
      alt="Third slide"
    />
  </Carousel.Item>
</Carousel>

			<Banner bannerProp={bannerData} />
			<Highlights />
		</>

	)

}