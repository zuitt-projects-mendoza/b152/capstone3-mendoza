import {useState,useEffect,useContext} from 'react';

import UserContext from '../userContext';

import {Table} from 'react-bootstrap';

export default function Orders(){

	const {user} = useContext(UserContext);

	const [allOrders,setAllOrders] = useState([]);

		useEffect(()=>{

			if(user.isAdmin){

				fetch('https://dry-citadel-77970.herokuapp.com/orders/',{
					headers: {
						'Authorization': `Bearer ${localStorage.getItem('token')}`
					}
				})
				.then(res => res.json())
				.then(data => {

					setAllOrders(data.map(order => {

						return (

							<tr key={order._id}>
								<td>{order._id}</td>
								<td>{order.purchasedOn}</td>
								<td>&#8369;{order.totalAmount}</td>
								<td>{}</td>	
							</tr>)

					}))

				})
			}

		},[user.isAdmin])

		return (

			<>
			<h1 className="mt-5 text-center">All Orders</h1>
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-light">
					<tr>
						<th>Order ID</th>
						<th>Purchase Date</th>
						<th>Total Amount</th>
						<th>Products</th>
					</tr>
				</thead>
				<tbody>
						{allOrders}
				</tbody>
			</Table>
			</>
		)

	}