import {useState,useEffect,useContext} from 'react';

import Banner from '../components/Banner';

import ProductCard from '../components/Product';

import AdminDashboard from '../components/AdminDashboard';

import UserContext from '../userContext';

import {Row} from 'react-bootstrap';

export default function Products(){

	const {user} = useContext(UserContext);

	let productsBanner = {

		title: "Welcome to the Products Page.",
		description: "View one of our Products below.",
		buttonText: "Register/Login to Order",
		destination: "/register"

	}

	const [productsArray,setProductsArray] = useState([])

	useEffect(()=>{

		fetch("https://dry-citadel-77970.herokuapp.com/products/getActiveProducts")
		.then(res => res.json())
		.then(data =>{

			setProductsArray(data.map(product => {

				return (

					<ProductCard key={product._id} productProp={product} />

					)
			}))

		})

	},[])

	return (

		user.isAdmin
		?
		<AdminDashboard />
		:
		<>
			<Banner bannerProp={productsBanner}/>
			<Row>{productsArray}</Row>
		</>

	)

}