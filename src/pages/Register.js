import {useState,useContext,useEffect} from 'react';
import {Form,Button,Row,Col} from 'react-bootstrap';

import Swal from 'sweetalert2';

import UserContext from '../userContext';

import {Navigate} from 'react-router-dom';

export default function Register(){

	const {user} = useContext(UserContext);

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");

	const [isActive,setIsActive] = useState(false);

	useEffect(()=>{

		if(firstName !== "" && lastName !== "" && email !== "" && mobileNo.length === 11 && password !== "" && confirmPassword !== "" && password === confirmPassword){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[firstName,lastName,email,mobileNo,password,confirmPassword])

	function registerUser(e){

		e.preventDefault();

		fetch('https://dry-citadel-77970.herokuapp.com/users/',{

			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({

				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password

			})
		})

		.then(res => res.json())
		.then(data => {

			console.log(data);
			
			if(data.email){
				Swal.fire({

					icon: "success",
					title: "Registration Successful",
					text: "Thank you for registering!"

				})

				window.location.href = "/login";
				
			} else {
				Swal.fire({

					icon: "error",
					title: "Registration Failed",
					text: "Something Went Wrong."

				})
			}

		})

	}

	return (
		user.id
		?
		<Navigate to="/" replace={true} />
		:
		<Row className="mt-5 justify-content-center">
			<Col xs={12} md={5}>
			<>
			<h1 className="my-5 text-center">Register</h1>
			<Form onSubmit={e =>registerUser(e)}>
				<Form.Group>
					<Form.Label>First Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter First Name" required value={firstName} onChange={e =>{setFirstName(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Last Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter Last Name" required value={lastName} onChange={e =>{setLastName(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Email:</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" required value={email} onChange={e =>{setEmail(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Mobile No:</Form.Label>
					<Form.Control type="number" placeholder="Enter 11 Digit No." required value={mobileNo} onChange={e =>{setMobileNo(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" required value={password} onChange={e =>{setPassword(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Confirm Password:</Form.Label>
					<Form.Control type="password" placeholder="Confirm Password" required value={confirmPassword} onChange={e =>{setConfirmPassword(e.target.value)}}/>
				</Form.Group>				
				{
					isActive
					?
					<Button variant="dark" type="submit" className="my-5">Submit</Button>
					:
					<Button variant="dark" type="submit" className="my-5" disabled>Submit</Button>
				}				
			</Form>
			</>
			</Col>
		</Row>

	)
}