import {useState,useEffect,useContext} from 'react';
import {Card,Button,Row,Col} from 'react-bootstrap';
import {useParams,Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../userContext'

export default function ViewProduct(){

	const {productId} = useParams();

	const {user} = useContext(UserContext);
	console.log(user);

	const [productDetails,setProductDetails] = useState({

		name: null,
		description: null,
		price: null,

	})

	useEffect(()=>{

		fetch(`https://dry-citadel-77970.herokuapp.com/products/getSingleProduct/${productId}`)
		.then(res => res.json())
		.then(data => {

			setProductDetails({

				name: data.name,
				description: data.description,
				price: data.price

			})

		})

	},[productId])

	/*function createOrder(){

		console.log("order");
		console.log(productId);

		let token = localStorage.getItem('token');

		fetch('http://localhost:4000/orders/',{

			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({

				productId: productId

			})

		})
		.then(res => res.json())
		.then(data => {

			console.log(data);
			if(data.message === "Ordered Successfully."){
				Swal.fire({

					icon: "success",
					title: "Ordered Successfully.",
					text: "Thank you for ordering."

				})
			} else {
				Swal.fire({

					icon: "error",
					title: "Ordering Failed.",
					text: data.message
					
				})
			}
		})

	}
*/
	return (

		<Row className="mt-5 justify-content-center">
			<Col xs={12} md={7}>
				<Card>
					<Card.Img variant="top" src="https://nayemdevs.com/wp-content/uploads/2020/03/default-product-image.png" />
					<Card.Body className="text-center">
						<Card.Title>{productDetails.name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{productDetails.description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>&#8369;{productDetails.price}</Card.Text>
					</Card.Body>
					{
						user.id && user.isAdmin === false
						? <Button variant="dark" className="btn-block">Order</Button>
						: <Link className="btn btn-danger btn-block" to="/login">Login To Order</Link>
					}
				</Card>
			</Col>
		</Row>

		)
}